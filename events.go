package latencyAt

import "time"

type Event struct {
	Key       string
	Value     string
	Timestamp time.Time
}

type EventService interface {
	Healthy() error
	Purge() error
	Get(key string) (*Event, error)
	Add(event *Event) error
	AddTx(event *Event) (Tx, error)
}

type Tx interface {
	Commit() error
	Rollback() error
}
