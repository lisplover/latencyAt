package subs

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
)

// Reads https://github.com/kdeldycke/vat-rates/blob/master/vat_rates.csv
func parseVatRates(filename string) (map[string]float64, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	r := csv.NewReader(file)
	rates := map[string]float64{}
	r.Read() // throw away header
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		ccs := strings.ToUpper(record[2])
		for _, cc := range strings.Split(ccs, "\n") {
			start, err := time.Parse("2006-01-02", record[0])
			if err != nil {
				return nil, err
			}
			now := time.Now()
			if start.After(now) {
				// Not yet active
				continue
			}
			if record[1] != "" {
				end, err := time.Parse("2006-01-02", record[1])
				if err != nil {
					return nil, err
				}
				if end.Before(now) {
					// Past period
					continue
				}
			}
			log.WithFields(log.Fields{
				"country": cc,
				"start":   record[0],
				"end":     record[1],
				"rate":    record[4],
			}).Debug("Using tax record")
			if rate, ok := rates[cc]; ok {
				return nil, fmt.Errorf("Duplicated rate %v, already got %v", record, rate)
			}
			rate, err := strconv.ParseFloat(record[4], 64)
			if err != nil {
				return nil, err
			}
			rates[cc] = rate
		}
	}
	return rates, nil
}
