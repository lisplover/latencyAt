package latencyAt

import "time"

type EmailToken struct {
	ID      int
	Token   string
	Created time.Time
}

type EmailTokenService interface {
	Healthy() error
	EmailToken(id int) (*EmailToken, error)
	EmailTokenByToken(token string) (*EmailToken, error)
	CreateEmailToken(t *EmailToken) error
	UpdateEmailToken(t *EmailToken) error
	DeleteEmailToken(t *EmailToken) error
	Purge() error
}
