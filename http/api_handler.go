package http

import (
	"crypto/subtle"
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"strings"

	"github.com/Sirupsen/logrus"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/stripe/stripe-go"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/latency.at/latencyAt"
	"gitlab.com/latency.at/latencyAt/errors"
)

type APIHandler struct {
	*httprouter.Router
	*logrus.Logger
	*latencyAt.Config

	latencyAt.MailService
	latencyAt.UserService
	latencyAt.EmailTokenService
	latencyAt.TokenService
	latencyAt.SubscriptionService

	*TokenAuth
}

func NewAPIHandler(cf *latencyAt.Config, us latencyAt.UserService, ms latencyAt.MailService, ets latencyAt.EmailTokenService, ta *TokenAuth, ts latencyAt.TokenService, ss latencyAt.SubscriptionService, stripeKey string) (*APIHandler, error) {
	stripe.Key = stripeKey
	h := &APIHandler{
		Router:              httprouter.New(),
		Logger:              logrus.New(),
		Config:              cf,
		UserService:         us,
		EmailTokenService:   ets,
		TokenAuth:           ta,
		MailService:         ms,
		TokenService:        ts,
		SubscriptionService: ss,
	}
	// FIXME: Rename user to 'account' maybe?
	h.Route("POST", "/api/user", h.handleSignup)
	h.Route("POST", "/api/user/login", h.handleLogin)
	h.Route("POST", "/api/user/activate/:token", h.TokenAuth.Require(h.handleActivate, false))
	h.Route("POST", "/api/user/reset/:token", h.handleReset)

	h.Route("POST", "/api/user/activate_mail", h.TokenAuth.Require(h.handleActivateMail, false))
	h.Route("POST", "/api/user/reset_mail", h.handleResetMail)

	h.Route("POST", "/api/user/password", h.TokenAuth.Require(h.handleChangePassword, true))
	h.Route("POST", "/api/user/email", h.TokenAuth.Require(h.handleChangeEmail, false))
	h.Route("POST", "/api/user/vatin", h.TokenAuth.Require(h.handleChangeVatIn, true))

	h.Route("GET", "/api/user/card", h.TokenAuth.Require(h.handleGetCard, false))
	h.Route("POST", "/api/user/card", h.TokenAuth.Require(h.handleAddCard, true))
	h.Route("DELETE", "/api/user/card", h.TokenAuth.Require(h.handleDeleteCard, true))

	h.Route("GET", "/api/user", h.TokenAuth.Require(h.handleUser, false))
	h.Route("GET", "/api/user/subscription", h.TokenAuth.Require(h.handleUserSubscription, false))
	h.Route("DELETE", "/api/user/subscription", h.TokenAuth.Require(h.handleUserUnsubscribe, false))

	h.Route("GET", "/api/tokens", h.TokenAuth.Require(h.handleGetTokens, false))
	h.Route("POST", "/api/tokens", h.TokenAuth.Require(h.handleCreateToken, true))
	h.Route("DELETE", "/api/tokens/:id", h.TokenAuth.Require(h.handleDeleteToken, true))

	h.Route("GET", "/api/plans", h.handleGetPlans)
	h.Route("GET", "/api/plan/tax/:country", h.TokenAuth.Require(h.handleGetTax, false))
	h.Route("POST", "/api/plan/:plan/subscribe", h.TokenAuth.Require(h.handleSubscribePlan, true))

	h.Route("POST", "/api/hooks/stripe", h.stripeHook)

	h.Route("POST", "/api/book/:token", h.handleBook)
	h.Handle("GET", "/api/metrics", h.handleMetrics)
	return h, nil
}

func (a *APIHandler) Route(method, path string, fn HandlerErrFunc) {
	a.Router.Handle(method, path, HandleError(method, path, fn))
}

func (a *APIHandler) HandleHealth(w http.ResponseWriter, r *http.Request) {
	for _, err := range []error{
		a.UserService.Healthy(),
		a.EmailTokenService.Healthy(),
		a.TokenService.Healthy(),
	} {
		if err != nil {
			Error(w, err, http.StatusInternalServerError)
			return
		}
	}

	a.encode(w, &latencyAt.StatusOK)
}

func (a *APIHandler) handleMetrics(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fields := strings.Fields(r.Header.Get("Authorization"))
	if len(fields) != 2 {
		http.Error(w, errors.ErrAuthorizationMissing.Error(), http.StatusBadRequest)
		return
	}
	if fields[0] != "Bearer" {
		http.Error(w, errors.ErrAuthorizationMissing.Error(), http.StatusBadRequest)
		return
	}

	var (
		tokenS       = fields[1]
		registry     = prometheus.NewRegistry()
		balanceGauge = prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: "lat",
			Subsystem: "account",
			Name:      "available_requests",
			Help:      "Remaining requests for this month",
		})
		renewalGauge = prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: "lat",
			Subsystem: "plan",
			Name:      "renewal_timestamp_seconds",
			Help:      "UNIX timestamp of next plan renewal",
		})
		requestsGauge = prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: "lat",
			Subsystem: "plan",
			Name:      "requests",
			Help:      "In plan included number of requests",
		})
	)
	registry.MustRegister(balanceGauge)
	registry.MustRegister(renewalGauge)
	registry.MustRegister(requestsGauge)

	token, err := a.TokenByToken(tokenS)
	if err != nil {
		if err == errors.ErrTokenNotFound {
			http.Error(w, err.Error(), http.StatusForbidden)
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}
	user, err := a.User(token.UserID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	balanceGauge.Set(float64(user.Balance))
	if user.StripeCustomerID != "" {
		sub, err := a.SubscriptionService.GetSubscription(user.StripeCustomerID)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		renewalGauge.Set(float64(sub.PeriodEnd))
		req, err := strconv.Atoi(sub.Plan.Meta["requests"])
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		requestsGauge.Set(float64(req))
	}

	h := promhttp.HandlerFor(registry, promhttp.HandlerOpts{})
	h.ServeHTTP(w, r)
}

func (a *APIHandler) handleBook(r *http.Request, ps httprouter.Params) (interface{}, error) {
	if a.Config.AuthUsername == "" || a.Config.AuthPassword == "" {
		return nil, errors.ErrAuthNotSet
	}
	username, password, ok := r.BasicAuth()
	if !ok {
		return nil, errors.ErrAuthRequired
	}

	if !(subtle.ConstantTimeCompare([]byte(username), []byte(a.Config.AuthUsername)) == 1 &&
		subtle.ConstantTimeCompare([]byte(password), []byte(a.Config.AuthPassword)) == 1) {
		// Return a 500 here for now, so we can just return status 1:1 to clients
		return nil, errors.ErrCredentialsUserInvalid
	}
	token, err := a.TokenByToken(ps.ByName("token"))
	if err != nil {
		if err == errors.ErrTokenNotFound {
			err = &errors.Error{Err: err, StatusCode: http.StatusForbidden}
		}
		return nil, err
	}
	user, err := a.User(token.UserID)
	if err != nil {
		return nil, err
	}
	if user.Balance <= 0 {
		return nil, &errors.Error{Err: errors.ErrBalanceLow, StatusCode: http.StatusPaymentRequired}
	}
	// THIS WAS MOVED TO BOOKER VIA PUBSUB
	// If a user is valid and has paid, we should do our best to account
	// and handle the request either way.
	// if err := a.UserService.Balance(user, -1); err != nil {
	//	log.Println(err)
	//}
	return &latencyAt.StatusOK, nil
}

func (a *APIHandler) encode(w io.Writer, v interface{}) error {
	return json.NewEncoder(w).Encode(v)
}
